// console.log("Hello World!");


// JAVASCRIPT SYNCHRONOUS AND ASYNCHRONOUS


/*
	Synchronous
	- synchronous codes runs in sequence. This means that each operation must wait for the previous one to complete before executing

	Asynchronous
	- runs in parallel. This means that an operation can occur while another one is still being processed.

	Asynchronous code execution is often preferrable in situations where execution can be blocked. Some examples of this are network requests, long-running calculations, file system operations, etc. using asynchronous cpde in the browser ensures the page remains responsive and the user experience is mostly unaffected


	JS by default is synchronous - runs from top to bottom



*/

// Example:

	console.log("Hello World");
	// consasole.log("Hello Again");

	// for (let i = 0; i <= 1000; i++) {
	// 	console.log(i);
	// }
		
	console.log("Hello it's me")


/*


	- API stands for application programming interface
	- an application programming interface is a particular set of codes that allows software programs to communicatge with each other
	- an api is the interface through wich you accesss someone else's code or through which someone else's code accesses yours

	Example:
		Google APIs
			https://developers.google.com/identity/sign-in/web/sign-in
		Youtube API
			https://developers.google.com/youtube/iframe_api_reference
		

*/


// FETCH API

	// console.log(fetch('https://jsonplaceholder.typicode.com/posts'));


/*	

A "promise" is an object that represents the evenetual completion (or failure) of an asynchronous function and it's resulting value

	A promise is in one of these three states:
	Pending:
		- Initial state, neither fulfilled nor rejected
	Fulfilled:
		- Operation was successfully completed
	Rejected:
		- Operation failed

	-By using the .then method, we can now check for the status of the promise
	-The "fetch" method will return a "promise" that resolves to be a "response" object
	-The ".then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"
	- Syntax:
		fetch('URL')
		.then((response)) +> {})

*/


	// fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status))


	// fetch('https://jsonplaceholder.typicode.com/posts')
	// // use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
	// .then((response) => response.json())

	// // Print the converted JSON value from "fetch" request using multiple .then method to create a promise chain
	// .then((json) => console.log(json))


	/*
	
	- The "async" and "await" keywords is another approach that can be used to achieve asynchronous codes
	- Used in functions to indicate which portions of code should be waited
	- Creates an asynchronous function


	*/


	// async function fetchData () {
	// 	// Waits for the "fetch" method to complete then stroes the value in the "result" variable
	// 	let result = await fetch ('https://jsonplaceholder.typicode.com/posts')
	// 	// Result returned by fetch is required as a promise
	// 	console.log(result);
	// 	// The returned "response" is an object
	// 	console.log(typeof result);


	// 	// We cannot access the content of the "response" by directly accessiong it's body property
	// 	console.log(result.body)
	// 	let json = await result.json();
	// 	console.log(json)
	// };


	// fetchData();


// GETTING A SPECIFIC POST
// Retrieves a specific post following thr Rest API (retrieve, /posts/id:, GET)


	// fetch('https://jsonplaceholder.typicode.com/posts/1')
	// .then((response) => response.json())
	// .then((json) => console.log(json));



/*
	POSTMAN

	url: https://jsonplaceholder.typicode.com/posts/1
	method: GET

*/

/*

CREATING POST
	Syntax:
		fetch('URL', options)
		.then((response) => {})
		.then((response) => {})

*/

// Creates a news post following the REST API (create, /post/:id, POST)
	// fetch('https://jsonplaceholder.typicode.com/posts', {
	// 	method: 'POST',
	// 	headers: {
	// 		'Content-Type': 'application/json'	
	// 	},
	// 	body: JSON.stringify({
	// 		title: 'New post',
	// 		body: 'Hello World',
	// 		userId: 1
	// 	})
	// })
	// .then((response) => response.json())
	// .then((json) => console.log(json))






/*
	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts
	method: POST
	body: 
				{
				    "title": "My First Blog Post",
				    "body": "Hello world!",
				    "userId": 1
				}

*/



// UPDATING A POST
// Updates a specific post following the REST API (update, /posts/:id, PUT)
	// fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// 	method: 'PUT',
	// 	headers: {
	// 		'Content-Type': 'application/json'
	// 	},
	// 	body: JSON.stringify({
	// 		id: 1,
	// 		title: 'Updated post',
	// 		body: 'Hello Again',
	// 		userId: 1

	// 	})

	// })

	// .then((response) => response.json())
	// .then((json) => console.log(json))



/*
	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts/1
	method: PUT
	body: raw + json
		{
			"title": "My First Revised Blog Post"
			"body": "Hello there! I revised this a bit.",
			"userId": 1
		}

*/



// Updates a post using the PATCH method

/*
	- Updates a specific post following the REST API (update, /posts/:id, Patch)

	- The differences between PUT and PATCH is the number of properties being changed

	-PATCH is used to update a single/several properties
	-PUT is used to update the whole object 

*/
	// fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// 	method: 'PATCH',
	// 	headers: {
	// 		'Content-Type': 'application/json'
	// 	},
	// 	body: JSON.stringify({
	// 		title: 'Corrected post',

	// 	})

	// })

	// .then((response) => response.json())
	// .then((json) => console.log(json));



	/*
		POSTMAN
		url: https://jsonplaceholder.typicode.com/posts/1
		method: PATCH
		body: raw + json
		{
			"title": "This is my final title"
		}


	*/


// DELETING A POST

// Deleting a specific post following the rest API (delete, /posts/:id, DELETE)

	// fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// 	method: 'DELETE',
	// })

	// .then((response) => response.json())
	// .then((json) => console.log(json));


/*
	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts/1
	method: DELETE

*/


// FILTERING THE POST

/*
	- The data can be filtered by sending the userId along with the URL
	- Information sent via the URL can be done by adding the question mark symbol (?)

	-Syntax:
		Individual Parameter:
		'url?parameterName=value'
		Multiple Parameters:
			'url?paramA=valueA&paramB=valueB'


*/



	// fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
	// .then((response) => response.json())
	// .then((json) => console.log(json));


	fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
	.then((response) => response.json())
	.then((json) => console.log(json));


// RETRIEVE COMMENTS OF A SPECIFIC POST
// Retrieving comments for a specific post following the Rest API (retrieve, /posts/id:, GET)

	fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then((response) => response.json())
	.then((json) => console.log(json));
